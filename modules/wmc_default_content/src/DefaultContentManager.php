<?php

namespace Drupal\wmc_default_content;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Default content manager service.
 */
class DefaultContentManager {

  /**
   * @var ModuleHandlerInterface
   */
  protected $module_handler;

  /**
   * @var DefaultContentInstaller
   */
  protected $default_content_installer;

  /**
   * Store default content configs defined in module hooks.
   *
   * @var array
   */
  protected $default_content = [];

  /**
   * Store uuid's which are already resolved during this request.
   *
   * @var array
   */
  protected $resolved_content = [];

  /**
   * @var string
   */
  protected $hook = 'default_content';

  /**
   * @var array
   */
  protected $hook_implementations = NULL;

  /**
   * Constructs a SystemManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param DefaultContentInstaller $default_content_installer
   */
  public function __construct(ModuleHandlerInterface $module_handler, DefaultContentInstaller $default_content_installer) {
    $this->module_handler = $module_handler;
    $this->default_content_installer = $default_content_installer;
  }

  /**
   * @param string $module
   * @param bool $uninstall
   * @return array|bool
   */
  public function resolveByModule($module, $uninstall = FALSE) {
    $configs = $this->getByModule($module);
    if (!is_null($configs)) {
      $results = [];
      foreach ($configs as $uuid => $config) {
        if ($this->isResolved($uuid)) {
          $results[$uuid] = TRUE;
          continue;
        }
        if ($uninstall === FALSE) {
          $result = $this->default_content_installer->install($uuid, $config);
        }
        else {
          $result = $this->default_content_installer->uninstall($uuid);
        }
        if ($result === TRUE) {
          $this->setResolved($uuid);
        }
        $results[$uuid] = $result;
      }
      return $results;
    }
    return FALSE;
  }

  /**
   * @param string $uuid
   * @param bool $uninstall
   * @return bool
   */
  public function resolveByUUID($uuid, $uninstall = FALSE) {
    if ($this->isResolved($uuid)) {
      return TRUE;
    }
    $config = $this->getByUUID($uuid);
    if (!is_null($config)) {
      if ($uninstall === FALSE) {
        $result = $this->default_content_installer->install($uuid, $config);
      }
      else {
        $result = $this->default_content_installer->uninstall($uuid);
      }
      if ($result === TRUE) {
        $this->setResolved($uuid);
      }
      return $result;
    }
    return FALSE;
  }

  /**
   * @param $module
   * @return array|NULL
   */
  protected function getByModule($module) {
    $config = NULL;
    foreach($this->getDefaultContent() as $key => $value) {
      if ($key === $module) {
        $config = $value;
        break;
      }
    }
    return $config;
  }

  /**
   * @param $uuid
   * @return array|NULL
   */
  protected function getByUUID($uuid) {
    $config = NULL;
    foreach ($this->getDefaultContent() as $module => $config) {
      foreach ($config as $config_uuid => $config_values) {
        if ($config_uuid === $uuid) {
          $config = $config_values += ['module' => $module];
          break 2;
        }
      }
    }
    return $config;
  }

  /**
   * @param $uuid
   * @return bool
   */
  protected function isResolved($uuid) {
    return isset($this->resolved_content[$uuid]) && $this->resolved_content[$uuid] === TRUE;
  }

  /**
   * @param $uuid
   * @param bool|TRUE $status
   * @return $this
   */
  protected function setResolved($uuid, $status = TRUE) {
    $this->resolved_content[$uuid] = $status;
    return $this;
  }

  /**
   * @return array
   */
  protected function getDefaultContent() {
    foreach ($this->getDefaultContentImplementations() as $module) {
      if (isset($this->default_content[$module])) {
        continue;
      }

      $function = $module . '_' . $this->hook;
      $result = call_user_func_array($function, []);
      $this->default_content[$module] = is_array($result) ? $result : [];
    }
    return $this->default_content;
  }

  /**
   * @return array
   */
  protected function getDefaultContentImplementations() {
    if (is_null($this->hook_implementations)) {
      $this->hook_implementations = $this->module_handler->getImplementations($this->hook);
    }
    return $this->hook_implementations;
  }

}
