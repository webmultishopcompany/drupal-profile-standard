<?php

namespace Drupal\wmc_straw_theme\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;

/** *
 * @Block(
 *   id = "logo",
 *   admin_label = @Translation("Logo")
 * )
 */
class Logo extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#markup' => '',
    ];

    $link_markup = '';

    $theme_logo_settings = theme_get_setting('logo');
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($language == 'lv') {
      $logo_url = $theme_logo_settings['url'];
    }
    elseif ($language == 'en') {
      $logo_file = \Drupal\file\Entity\File::load(theme_get_setting('logo_en')[0]);
      $logo_uri = $logo_file->getFileUri();
      $logo_url = file_create_url($logo_uri);
    }
    elseif ($language == 'ru') {
      $logo_file = \Drupal\file\Entity\File::load(theme_get_setting('logo_ru')[0]);
      $logo_uri = $logo_file->getFileUri();
      $logo_url = file_create_url($logo_uri);
    }
    else {
      $logo_url = '';
    }
    if (!is_null($theme_logo_settings)) {
      $link_markup .= '<img src="' . $logo_url . '" alt="' . t('Logo') . '" class="img-responsive logo-main ' . $language . '" />';
    }

    $build['#markup'] .= Link::createFromRoute([
      '#markup' => $link_markup,
    ], '<front>')->toString();

    return $build;
  }
}
