<?php

namespace Drupal\wmc_default_content;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default content installer service.
 */
class DefaultContentInstaller {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * Constructs a SystemManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entity_type_manager = $entity_type_manager;
  }

  /**
   * @param $computed_uuid string
   * @param $config array
   * @return bool
   */
  public function install($computed_uuid, $config) {
    list($entity_type_id, $uuid) = explode(':', $computed_uuid);

    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage */
    $entity_storage = $this->entity_type_manager->getStorage($entity_type_id);
    $existing_entities = $entity_storage->loadByProperties([
      'uuid' => $uuid,
    ]);

    if (empty($existing_entities)) {
      switch ($entity_type_id) {
        case 'file':
          $result = $this->doInstallFile($entity_storage, $uuid, $config);
          break;
        default:
          $result = $this->doInstall($entity_storage, $uuid, $config);
      }
      return $result;
    }

    return FALSE;
  }

  /**
   * @param $entity_storage \Drupal\Core\Entity\EntityStorageInterface
   * @param $uuid string
   * @param $config array
   * @return bool
   */
  protected function doInstall($entity_storage, $uuid, $config) {
    $entity = $entity_storage->create($config + ['uuid' => $uuid]);
    $result = $entity->save();
    return $result !== FALSE;
  }

  /**
   * @param $entity_storage \Drupal\Core\Entity\EntityStorageInterface
   * @param $uuid string
   * @param $config array
   * @return bool
   */
  protected function doInstallFile($entity_storage, $uuid, $config) {
    if (!isset($config['file_path'])) {
      return FALSE;
    }

    // @todo specify default content folder when error is resolved:
    //  - The specified file %file could not be moved/copied because the destination directory is not properly configured.
    $uri = file_unmanaged_copy($config['file_path'], 'public://' . basename($config['file_path']));
    if ($uri === FALSE) {
      return FALSE;
    }

    $config['uri'] = $uri;

    return $this->doInstall($entity_storage, $uuid, $config);
  }

  /**
   * @param $computed_uuid string
   * @return bool
   */
  public function uninstall($computed_uuid) {
    list($entity_type_id, $uuid) = explode(':', $computed_uuid);

    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage */
    $entity_storage = $this->entity_type_manager->getStorage($entity_type_id);
    /** @var \Drupal\Core\Entity\EntityInterface[] $existing_entities */
    $existing_entities = $entity_storage->loadByProperties([
      'uuid' => $uuid,
    ]);

    if (!empty($existing_entities)) {
      $result = $this->doUninstall(reset($existing_entities));
      return $result;
    }

    return FALSE;
  }

  /**
   * @param $entity \Drupal\Core\Entity\EntityInterface
   * @return bool
   */
  protected function doUninstall($entity) {
    try {
      $entity->delete();
    }
    catch (EntityStorageException $e) {
      return FALSE;
    }
    return TRUE;
  }

}
