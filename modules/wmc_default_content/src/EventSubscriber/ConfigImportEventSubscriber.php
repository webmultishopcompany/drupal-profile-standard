<?php

namespace Drupal\wmc_default_content\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to the missing content event.
 *
 * Ensure that all missing content dependencies are installed on config import.
 *
 * @see \Drupal\Core\Config\ConfigImporter::processMissingContent()
 */
class ConfigImportEventSubscriber implements EventSubscriberInterface {

  /**
   * Handles the missing content event.
   *
   * @param \Drupal\Core\Config\Importer\MissingContentEvent $event
   *   The missing content event.
   */
  public function onMissingContent(MissingContentEvent $event) {
    $hook = 'config_import_missing_content';
    $module_handler = \Drupal::moduleHandler();
    $implementations = $module_handler->getImplementations($hook);

    foreach ($event->getMissingContent() as $uuid => $content) {
      $resolved_uuid = $this->resolveMissingContent($hook, $implementations, $content);
      if (is_string($resolved_uuid)) {
        $event->resolveMissingContent($resolved_uuid);
      }
    }
  }

  /**
   * @param $hook
   * @param $implementations
   * @param $content
   * @return null|string
   */
  protected function resolveMissingContent($hook, $implementations, $content) {
    $uuid = NULL;
    foreach ($implementations as $module) {
      $function = $module . '_' . $hook;
      $result = call_user_func_array($function, $content);
      if (isset($result) && is_string($result)) {
        $uuid = $result;
        break;
      }
    }
    return $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::IMPORT_MISSING_CONTENT][] = [
      'onMissingContent',
      100,
    ];
    return $events;
  }

}